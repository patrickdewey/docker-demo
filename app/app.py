from flask import Flask, request, render_template
import requests

app = Flask(__name__)

# Define the URL of the data gathering container
joke_url = "http://jokes-api:5000/get-joke"

def get_random_joke():
    try:
        response = requests.get(joke_url)
        response.raise_for_status()
        joke_data = response.json()
        setup = joke_data["setup"]
        punchline = joke_data["punchline"]
        joke = setup + "\n" + punchline
    except Exception as e:
        joke = f"Failed to fetch a joke. Error: {str(e)}"
    return joke

@app.route('/')
def index():
    joke = get_random_joke()
    return render_template('index.html', joke=joke)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80)
