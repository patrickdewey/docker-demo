# Tools and Techniques Presentation 2023 - Docker Demo
By: Patrick Dewey (Team Floodwig Van Riverflowin)

## Background
Docker is a very powerful tool both in data science and software engineering that allows for better portability, reproducibility, and scalability of applications. 
It does this by providing a way to develop and wrap everything needed for an application into a package called a container.
Containers are lightweight, standalone executable environments that contain both application code and all required dependencies and libraries.
Containers can be easily created and deployed, and can also be linked with other containers for more complicated applications.


### Installation
- Download Docker: https://docs.docker.com/get-docker/  

### How to Run
Webapp Demo (basic webserver with automated acquisition and multiple containers):
1. Open up your terminal and change into this directory `cd path/to/project/directory`
2. Build, run, and link the containers defined in [docker-compose.yml](docker-compose.yml) using the command: `docker compose up -d`
3. Open `localhost:8080/` to view the basic webpage.
4. Run `docker compose down` to stop the container and clean up afterwards.

### Next Steps
For more information about Docker, follow the [getting started guide](https://docs.docker.com/get-started/).
