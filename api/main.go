package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

func main() {
	http.HandleFunc("/get-joke", fetchJoke)
	http.ListenAndServe(":5000", nil)
}

func fetchJoke(w http.ResponseWriter, r *http.Request) {
	jokeAPIURL := "https://official-joke-api.appspot.com/jokes/random"

	resp, err := http.Get(jokeAPIURL)
	if err != nil {
		joke := fmt.Sprintf("Failed to fetch a joke. Error: %s", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(joke))
		return
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}

	jokeData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		joke := fmt.Sprintf("Failed to read the joke. Error: %s", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(joke))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(jokeData)
}
